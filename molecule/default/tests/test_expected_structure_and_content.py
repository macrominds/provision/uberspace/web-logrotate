def test_expected_structure(host):
    file = host.file('/home/ansible/etc/web-logrotate.conf')
    assert file.exists
    assert 'ansible' == file.user
    assert 'ansible' == file.group
    assert 0o644 == file.mode


def test_expected_key_content(host):
    assert get_expected_conf_content() \
           == host.file('/home/ansible/etc/web-logrotate.conf') \
           .content_string.strip()


def get_expected_conf_content():
    return """weekly

/var/www/virtual/ansible/shared/log/*.log {
    weekly
    missingok
    rotate 8
}"""
