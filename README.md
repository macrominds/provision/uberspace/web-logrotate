# macrominds/provision/uberspace/web-logrotate

Setup logrotate for macrominds/website on an uberspace. 

## Requirements

Setup an [U7 Uberspace](https://dashboard.uberspace.de/register) and provide an ssh access to it.

## Role Variables

See [defaults/main.yml](defaults/main.yml). 

* `web_logrotate_bin`: Path to the logrotate executable 
  Defaults to `/usr/sbin/logrotate`
* `web_logrotate_config_src`: Path to the local configuration template
  Defaults to `templates/macrominds-website.conf`
* `web_logrotate_config_path`: Remote base path of the configuration 
  Defaults to `/home/{{ ansible_facts.user_id }}/etc`
* `web_logrotate_config_dest`: Remote location of the configuration
  Defaults to `{{ web_logrotate_config_path}}/web-logrotate.conf`
* `web_logrotate_logfile_glob`: Globbing for the log files to rotate
  Defaults to `/var/www/virtual/{{ ansible_facts.user_id }}/shared/log/*.log`
* `web_logrotate_status_file`: Status file to use (required when you run logrotate not as root)
  Defaults to `/home/{{ ansible_facts.user_id }}/.logrotate.status`

## Example Playbook

### Prerequisites

In your project, provide the following files:

ansible.cfg

```ini
[defaults]
roles_path = $PWD/galaxy_roles:$PWD/roles
```

requirements.yml
 
```yaml
- src: git+https://gitlab.com/macrominds/provision/uberspace/web-logrotate.git
  path: roles
  name: web-logrotate
```

And run `ansible-galaxy install -r requirements.yml` to install 
or `ansible-galaxy install -r requirements.yml  --force` to upgrade.

### Playbook

```yaml
- hosts: all
  roles:
    - role: web-logrotate
```

## Testing

Test this role with `molecule test --all`.

## License

ISC

## Author Information

This role was created in 2019 by Thomas Praxl.
